"""
Copyright 2022 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
ASTRON Netherlands Institute for Radio Astronomy
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


 Created: 2022-12-07
This file contains the definitions of the APSPU registers etc.


"""
#
# I2C address, registers and ports for APSCT
# Created: 2023-01-06
#

#
# Power supplies
#

PWR_LOCATIONS = {"CLK_IN":  0,
                 "PLL": 1,
                 "OCXO": 2,
                 "CLK_DIST": 3,
                 "PPS_IN": 4,
                 "PPS_DIST": 5,
                 "CTRL": 6}

PWR_VOUT = {"CLK_IN":  3.3,
            "PLL": 3.3,
            "OCXO": 3.3,
            "CLK_DIST": 3.3,
            "PPS_IN": 3.3,
            "PPS_DIST": 3.3,
            "CTRL": 3.3}

#
# PLL constants / pin locations
#
CS = 6
SCLK = 4
SDO = 5
SDI = 7

PLL = 0x20

#
# Central I2C Devices
#
EEPROM = 0x50


#
# ID Pins 
#
ID_PINS = [8, 7, 12, 16, 20, 21]
