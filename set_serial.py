'''
Set RCU2 version via PCC. RCU number from commandline

'''
import sys
import time
sys.path.insert(0,'.')
import os
if os.name == "posix":
    from I2C_serial_pi import *
else:
    from I2C_serial import *

sleep_time=0.05

I2CBUSNR=int(sys.argv[1]) if len(sys.argv)>1 else 0
version=sys.argv[2] if len(sys.argv)>2 else ""

REGISTER=int(sys.argv[3]) if len(sys.argv)>3 else 0

EEPROM_ADDR = 0x50
#I2CBUSNR = 1
LEN=16
#REGISTER=0x20



def rw_eeprom(value):
    I2C_eeprom = I2C(EEPROM_ADDR, BUSNR=I2CBUSNR)
    if len(value)>0:
       for i,v in enumerate(bytearray(value)):
         print(i,hex(v))
         I2C_eeprom.write_bytes(i+REGISTER,v)
         sleep(0.1)
       for i in range(len(value),LEN):
         I2C_eeprom.write_bytes(i+REGISTER,0xff)
         sleep(0.1)
    v2=[]
    sleep(0.1)
    I2C_eeprom.write_register(REGISTER)
    sleep(0.1)
    ret_ack, ret_value = I2C_eeprom.read_last_reg(LEN)
    if ret_ack:
        stri = "EEPROM readback : {} ".format(ret_value)
        print(stri)
    else:
        print("READBACK failed")

if len(version)>LEN: version=version[:LEN]
print("Set I2C bus  %i: version= '%s'" % (I2CBUSNR,version));

rw_eeprom(version)

