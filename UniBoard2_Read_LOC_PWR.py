#******************************************#
# Application: JFT_1
# Created: 2015-05-21, 09:14:44
#******************************************#

#******************************************#
# Imports
#******************************************#
# Write your Imports here.

import sys
import time
sys.path.insert(0,'.')
import os
if os.name =="posix":
    from I2C_serial_pi import *
else:
    from I2C_serial import *

from UniBoard2_I2C import *
I2CBUSNR=3


LOC_PWR = I2C(0x53)
LOC_PWR.bus = I2CBUSNR
ret_ack, ret_value = LOC_PWR.read_bytes(0)
if  ret_ack < 1:
    print("no device found")
else:
    pr_stri = "Found device at address 0x{:02x}".format(LOC_PWR.I2C_Address)
    print(pr_stri)
    value=[]
    ret_ack,vout_mod = LOC_PWR.read_bytes(0x00, 1)
#    ret_ack,ret_value = LOC_PWR.read_bytes(LP_VOUT, 2)
#    vout = calc_lin_3bytes(ret_value, vout_mod)
    print("read = ", vout_mod)
#    ret_ack,ret_value = LOC_PWR.read_bytes(LP_IOUT, 2)
#    iout = calc_lin_2bytes(ret_value)
#    print("Output Current :",iout)
#    ret_ack,ret_value = LOC_PWR.read_bytes(LP_temp, 2)
#    temp = calc_lin_2bytes(ret_value)
#    print("temperature :",temp)
