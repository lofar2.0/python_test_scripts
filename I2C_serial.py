'''
I2C Class

'''
import sys
sys.path.insert(0,'c:\python34\lib\site-packages')
import serial
from time import * 

DEBUG=False

ser = serial.Serial()
ser.baudrate = 57600
ser.port = 'COM4'
ser.parity = serial.PARITY_NONE
ser.timeout = 1

class I2C:

    def __init__(self, ADDRESS='040'):
        self.I2C_Address = ADDRESS
        self.BUS_NR = 1 # not used for Laptop but to enable Pi multiple busses.


#    def chk_ack(self):
#        return ret_ack
#    def write_bytes(self,register,data):
#        return ret_ack
#    def write_last_reg(self,data):
#        return ret_ack
        
    def read_bytes(self, register, bytes_to_read=2):
        serial_string='S{0:x}{1:{fill}2x}PS{2:x}{3:02}45P'.format(self.I2C_Address*2, register, (self.I2C_Address*2+1), bytes_to_read, fill = '0')
        serial_string=serial_string.upper()
        ret_ack = 1
        if DEBUG:
            print(serial_string)
            ret_value="0F"
        else:
            ser.open()
            ser.write(bytes(serial_string, 'utf-8'))
            ret_value = ser.read(bytes_to_read*2) 
            ser.close()
            ret_value = ret_value.decode("utf-8") 
        return ret_ack, ret_value

    
    def read_last_reg(self, bytes_to_read):
        serial_string='S{0:x}{1:02}P'.format((self.I2C_Address*2+1), bytes_to_read)
        serial_string=serial_string.upper()
        ret_ack = 1
        if DEBUG:
            print(serial_string)
            ret_value="0F"
        else:
            ser.open()
            ser.write(bytes(serial_string, 'utf-8'))
            ret_value = ser.read(bytes_to_read*2) 
            ser.close()
            ret_value = ret_value.decode("utf-8") 
        return ret_ack,ret_value
  
    def write_bytes(self, register, data):
        ret_value=[]
        serial_string='S{0:x}{1:02x}{2:02x}P'.format(self.I2C_Address*2, register, data)
        serial_string=serial_string.upper()
        ret_ack = 1
        if DEBUG:
            print(serial_string)
        else:
            ser.open()
            ser.write(bytes(serial_string, 'utf-8'))
            ser.close()
        return ret_ack

    def write_pointer(self, register):
        ret_value=[]
        serial_string='S{0:x}{1:02x}P'.format(self.I2C_Address*2, register)
        serial_string=serial_string.upper()
        ret_ack = 1
        if DEBUG:
            print(serial_string)
        else:
            ser.open()
            ser.write(bytes(serial_string, 'utf-8'))
            ser.close()
        return ret_ack

if __name__ == "__main__":
    I2C_Device = I2C(0x40)
    I2C_Device.write_bytes(0x00, 0x00)
    ret_ack, ret_value = I2C_Device.read_bytes(0x8C, 2)
    print(ret_value)
