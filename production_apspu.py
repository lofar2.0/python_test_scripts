"""
Copyright 2022 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
ASTRON Netherlands Institute for Radio Astronomy
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


 Created: 2022-12-07
This file contains the APSPU production script.

"""

import sys
sys.path.insert(0, '.')

from apspu_lib import *


if len(sys.argv) < 2:
    print("Production_apspy.py <ASTRON NR> <Serial number>")
    print("e.g. python production_apspy.py 2022-01 1234")
    exit()

apspu = ApspuClass()
apspu.apspu_on_off(False)
sleep(5)  # Wait for outputs to be stable off
apspu.set_pols()
input("Change dipswitches and press key to continue..")
apspu.apspu_on_off(True)
sleep(5)  # Wait for outputs to be stable on
apspu.read_all()
apspu.print_status()
if apspu.check_apspu():
    apspu_id = "APSPU-" + sys.argv[1]
    serial = sys.argv[2]
    rw_ok = apspu.eeprom.wr_rd_eeprom(apspu_id, address=0)
    if rw_ok:
        rw_ok = apspu.eeprom.wr_rd_eeprom(serial, address=0x20)
    if not rw_ok:
        print("EEPROM Error")
