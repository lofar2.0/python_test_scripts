"""
Copyright 2021 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
ASTRON Netherlands Institute for Radio Astronomy
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


  Created: 2021-05-10
This file contains the UniBoard2 class with all monitoring function. It can be used stand-alone to test it.

"""

import sys
sys.path.insert(0, '.')
import os
# import time
from UniBoard2_I2C import *
if os.name == "posix":
    from I2C_serial_pi import *
else:
    from I2C_serial import *


I2CBUSNR = 3
DEBUG = True #False

class Unb2cClass:
    #
    # Class that contains all parts on a UniBoard2
    #
    def __init__(self):
        self.status = False
        self.nodes = []
        self.pols = []
        for node_cnt in range(4):
            self.nodes.append(NodeClass(node_cnt))
        for pol in list(CTR_POLS.keys()):
            self.pols.append(PolClass(pol, location = "central"))
        self.dev_i2c_eeprom = I2C(EEPROM)
        self.dev_i2c_eeprom.bus_nr = I2CBUSNR
        self.main_switch = I2C(MAIN_I2C_SWITCH)
        self.main_switch.bus_nr = I2CBUSNR
        self.front = I2C(LED_DRIVER)
        self.front.bus_nr = I2CBUSNR

    def write_eeprom(self, data=0x01):
        #
        # Write the EEPROM with the serial number etc.
        #
        ret_ack, ret_value = self.dev_i2c_eeprom.read_bytes(0)
        if ret_ack < 1:
            if DEBUG:
                print("no device found")
            return False
        else:
            self.dev_i2c_eeprom.write_bytes(0x00, data)
            return True

    def read_eeprom(self):
        #
        # Read the EEPROM with the serial number etc.
        #
        ret_ack, ret_value = self.dev_i2c_eeprom.read_bytes(0)
        if ret_ack < 1:
            if DEBUG:
                print("no EEPROM found")
            return False
        else:
            ret_ack, ret_value = self.dev_i2c_eeprom.read_bytes(0x00, 1)
            return ret_value

    def wr_rd_eeprom(self, value=0x34):
        #
        # Write and Read the EEPROM to check functionality
        #
        self.write_eeprom(value)
        ret_value = self.read_eeprom()
        stri = "Wrote to EEPROM: 0x{0:X}, Read from EEPROM: 0x{1} ".format(value, ret_value)
        print(stri)
        return True

    def front_led(self, color):
        #
        # Write the LED on the front panel,
        # Input to this function is the color see UniBoard_i2c.py
        #
        # select LED
        ret_ack = self.main_switch.write_bytes(0x20, 0x20)
        if ret_ack < 1:
            if DEBUG:
                print("Main I2C switch not found")
            return False
        else:
            ret_ack = self.front.write_bytes(0x03, 0)
            if ret_ack < 1:
                if DEBUG:
                    print("Front LED driver not found")
            else:
                self.front.write_bytes(0x01, color)
            return True

    def read_all(self):
        #
        # Function to read all monitoring points of the UniBoard
        #
        for node in self.nodes:
            node.read_all()
        for pol in self.pols:
            pol.read_all()
        return True
    
    def read_temp(self):
        #
        # Function to read all monitoring points of the UniBoard
        #
        for node in self.nodes:
            node.read_temp()
        for pol in self.pols:
            pol.read_temp()
        return True


    def print_status(self):
        #
        # Function to dump monitoring information on the screen
        #
#        for color in list(LED_COLORS.keys()):
#            print(color)
#            self.front_led(LED_COLORS[color])
        for node in self.nodes:
            node.print_status()
        print("Status central UniBoard2 components")
        for pol in self.pols:
            pol.print_status()
#        self.wr_rd_eeprom()
        return True


class NodeClass:
    #
    # Class that contains all monitoring points of one FPGA node.
    #

    def __init__(self, number):
        #
        # All the monitoring points.
        #
        self.main_switch = I2C(MAIN_I2C_SWITCH)
        self.main_switch.bus_nr = I2CBUSNR
        self.node_switch = I2C(NODE_I2C_SWITCH)
        self.node_switch.bus_nr = I2CBUSNR
        self.node_number = number
        self.pols = []
        self.set_i2c_switches()
        for pol in list(LOC_POLS.keys()):
            self.pols.append(PolClass(pol))
        self.ddr = []
        for bank_cnt in range(2):
            self.ddr.append(DdrClass(bank_cnt))
        self.qsfp = []
        for qsfp_cnt in range(6):
            self.qsfp.append(QsfpClass(qsfp_cnt))

    def read_all(self):
        #
        # Function to read all monitoring points of one FPGA node
        #
        self.set_i2c_switches()
        for pol in self.pols:
            pol.read_all()
        for ddr in self.ddr:
            ddr.read_all()
        for qsfp in self.qsfp:
            qsfp.read_all()

    def read_temp(self):
        #
        # Function to read all monitoring points of one FPGA node
        #
        self.set_i2c_switches()
        for pol in self.pols:
            pol.read_temp()
        for ddr in self.ddr:
            ddr.read_all()


    def print_status(self):
        #
        # Function to dump all monitoring points of one FPGA node on the screen
        #
        stri = "Status of Node {0}".format(self.node_number)
        print(stri)
        for pol in self.pols:
            pol.print_status()
        for ddr in self.ddr:
            ddr.print_status()
#        for qsfp in self.qsfp:
#            qsfp.print_status()

    def set_i2c_switches(self):
        #
        # Before a node can ba accessed the i2c switches have to be set, this function can be used.
        #
        ret_ack = self.main_switch.write_bytes(0x0, 0x01 << self.node_number)  # select Node
        if ret_ack < 1:
            if DEBUG:
                print("Main I2C switch not found")
        else:
            ret_ack = self.node_switch.write_bytes(0x0, 0x20)  # select DDR4
            if ret_ack < 1:
                if DEBUG:
                    print("Node I2C switch not found")


class QsfpClass:
    #
    # Class that contains all monitoring points of a QSFP cage
    #
    def __init__(self, port):
        #
        # All the monitoring points of a QSFP
        #
        self.port = port
        self.temp = 0
        self.volt = 0
        self.status = False
        self.node_switch = I2C(NODE_I2C_SWITCH)
        self.node_switch.bus_nr = I2CBUSNR
        self.select_qsfp()
        self.qsfp_cage = I2C(QSFP_I2C_ADDR)
        self.qsfp_cage.bus_nr = I2CBUSNR

    def select_qsfp(self):
        #
        # Function to set the I2C switch to access a QSFP cage
        #
        ret_ack = self.node_switch.write_bytes(0x0, QSFP_PORT[self.port])
        if ret_ack < 1:
            if DEBUG:
                print("Node I2C switch not found")
            self.status = False
        else:
            self.status = True

    def read_temp(self):
        #
        # Function to read the temperature of a QSFP cage
        #
        self.select_qsfp()
        ret_ack, raw_ret = self.qsfp_cage.read_bytes(QSFP_TEMP, 2)
        if ret_ack < 1:
            if DEBUG:
                stri = "No QSFP found in port {0}".format(self.port)
                print(stri)
            self.status = False
        else:
            if (raw_ret[0:2] == 'ff') | (len(raw_ret) < 4):
                 self.status = False
            else:
                ret_value = []
                ret_value.append(int(raw_ret[:2], 16))
                ret_value.append(int(raw_ret[2:], 16))
                self.temp = (ret_value[0] * 256 + ret_value[1]) / 256
                self.status = True

    def read_volt(self):
        #
        # Function to read the power input of a QSFP cage
        #
        self.select_qsfp()
        ret_ack, raw_ret = self.qsfp_cage.read_bytes(QSFP_VOLT, 2)
        if ret_ack < 1:
            if DEBUG:
                stri = "No QSFP found in port {0}".format(self.port)
                print(stri)
            self.status = False
        else:
            if (raw_ret[0:2] == 'ff') | (len(raw_ret) < 4):
                self.status = False
            else:
                ret_value = []
                ret_value.append(int(raw_ret[:2], 16))
                ret_value.append(int(raw_ret[2:], 16))
                self.volt = (ret_value[0] * 256 + ret_value[1]) * 0.0001
                self.status = True

    def read_all(self):
        #
        # Function to read all monitoring points of a QSFP cage
        #
        self.read_temp()
        self.read_volt()

    def print_status(self):
        #
        # Function to dump all monitoring information of a QSFP cage on the screen
        #
#        if self.status:
        stri = "Slot {0} : QSFP Temperature QSFP  {1:3.2f} gr. C  Voltage {2:3.2f} V".format(self.port, self.temp, self.volt)
        print(stri)


class DdrClass:
    #
    # Class to read all monitoring points of a DDR4 module
    #
    def __init__(self, bank):
        #
        # All monitoring points of a DDR4 module
        #
        self.bank = bank
        self.status = False
        self.temp = 0
        self.node_switch = I2C(NODE_I2C_SWITCH)
        self.node_switch.bus_nr = I2CBUSNR
        if self.bank == 0:
            self.ddr_dev = I2C(MB_I_TEMP_I2C_ADDR)
        else:
            self.ddr_dev = I2C(MB_II_TEMP_I2C_ADDR)
        self.ddr_dev.bus_nr = I2CBUSNR


    def set_i2c_switch(self):
        #
        # Function to set the I2C switch to access the DDR4 modules
        #
        ret_ack = self.node_switch.write_bytes(0x0, DDR4)
        if ret_ack < 1:
            if DEBUG:
                print("Node I2C switch not found")
            self.status = True
        else:
            self.status = False

    def read_temp(self):
        #
        # Function to read the temperature of a DDR4 module
        #
        ret_ack, raw_ret = self.ddr_dev.read_bytes(MB_TEMP_REG, 2)
        if ret_ack < 1:
            if DEBUG:
                stri = "No DDR module in slot {0}".format(self.bank)
                print(stri)
            self.status = False
        else:
            ret_value = []
            ret_value.append(int(raw_ret[:2], 16))
            ret_value.append(int(raw_ret[2:], 16))
            self.temp = (((ret_value[0] & 0x1F) * 0x100) + (ret_value[1] & 0xFC)) * 0.0625
            self.status = True

    def read_all(self):
        #
        # Function to read all monitoring points of a DDR4 module
        #
        self.set_i2c_switch()
        self.read_temp()

    def print_status(self):
        #
        # Function to dump all monitoring points of a DDR4 module on the screen
        #
        if self.status:
            stri = "Temperature DDR4 in slot {0} is {1:3.2f} C".format(self.bank, self.temp)
            print(stri)


class PolClass:
    #
    # Class to read all monitoring points Point of Load DC/DC converter
    #
    def __init__(self, name, location="node"):
        #
        # All monitoring points Point of Load DC/DC converter
        #
        self.name = name
        self.location = location
        self.vout = 0
        self.iout = 0
        self.temp = 0
        self.node_switch = I2C(NODE_I2C_SWITCH)
        self.node_switch.bus_nr = I2CBUSNR
        self.main_switch = I2C(MAIN_I2C_SWITCH)
        self.main_switch.bus_nr = I2CBUSNR
        self.set_i2c_switch()
        if self.location == "node":
            self.pol_dev = I2C(LOC_POLS[self.name])
        else:
            self.pol_dev = I2C(CTR_POLS[self.name])
        self.pol_dev.bus_nr = I2CBUSNR
        ret_ack, ret_value = self.pol_dev.read_bytes(1)
        if ret_ack < 1:
            if self.location == "node": # DEBUG:
                stri = " Device {0} at address 0x{1:X} not found".format(self.name, LOC_POLS[self.name])
            else:
                stri = " Device {0} at address 0x{1:X} not found".format(self.name, CTR_POLS[self.name])
            print(stri)
            self.status = False
        else:
            self.status = True

    def set_i2c_switch(self):
        #
        # Function to set the I2C switch to access the Point of Load DC/DC converter
        #
        if self.location == 'node':
            ret_ack = self.node_switch.write_bytes(0x0, LOC_POWER)
            if ret_ack < 1:
                if DEBUG:
                    print("Node I2C switch not found")
                self.status = False
            else:
                self.status = True
        else:
            ret_ack = self.main_switch.write_bytes(0x10, 0x10)
            if ret_ack < 1:
                if DEBUG:
                    print("Main I2C switch not found")
                self.status = False
            else:
                self.status = True
                return False

    def read_vout(self):
        #
        # Function to read the output voltage of the Point of Load DC/DC converter
        #
        if self.status:
            if type == "node":
                self.set_i2c_switch()
            vout = 999
            cnt = 0
            while (vout > 10) & (cnt < 10):
                ret_ack, vout_mod = self.pol_dev.read_bytes(LP_VOUT_MODE, 1)
                ret_ack, raw_value = self.pol_dev.read_bytes(LP_VOUT, 2) 
                vout_mod = int(vout_mod, 16)
                ret_value = []
                ret_value.append(int(raw_value[:2], 16))
                ret_value.append(int(raw_value[2:], 16))
                vout = calc_lin_3bytes(ret_value, [vout_mod])
                cnt += 1
            self.vout = vout
        else:
            self.vout = 999
 
    def read_iout(self):
        #
        # Function to read the output current of the Point of Load DC/DC converter
        #
        if self.status:
            self.set_i2c_switch()
            iout = 999
            cnt = 0
            while (iout > 200) & (cnt < 10):
                ret_ack, raw_value = self.pol_dev.read_bytes(LP_IOUT, 2)
                ret_value = []
                ret_value.append(int(raw_value[:2], 16))
                ret_value.append(int(raw_value[2:], 16))
                iout = calc_lin_2bytes(ret_value)
                cnt += 1
            self.iout = iout
        else:
            self.iout = 999

    def read_temp(self):
        #
        # Function to read the temperature of the Point of Load DC/DC converter
        #
        if self.status:
            self.set_i2c_switch()
            temp = 999
            cnt = 0
            while (temp > 160) & (cnt < 25):
                ret_ack, raw_value = self.pol_dev.read_bytes(LP_temp, 2) 
                ret_value = []
                ret_value.append(int(raw_value[:2], 16))
                ret_value.append(int(raw_value[2:], 16))
                temp = calc_lin_2bytes(ret_value)
                cnt += 1
            if cnt < 9:
                self.temp = temp
        else:
            self.temp = 999

    def read_all(self):
        #
        # Function to read all monitoring points of the Point of Load DC/DC converter
        #
        self.read_vout()
        self.read_iout()
        self.read_temp()

    def print_status(self):
        #
        # Function to dump all monitoring points of the Point of Load DC/DC converter on the screen
        #
        if self.status:
            stri = "POL: " + self.name + "  "
            stri += "Output voltage :{0: <5.2f} V ".format(self.vout)
            stri += "Output Current :{0: <5.2f} A ".format(self.iout)
            stri += "temperature :{0: <5.2f} Deg C".format(self.temp)
            print(stri)


def main():
    #
    # Function to test the class, read all info and dump on the screen
    #
    unb = Unb2cClass()
    if unb.wr_rd_eeprom(value=0x34):
        while True:
            print("---------- READ All -----------")
            unb.read_all()
            unb.print_status()
            for cnt in range(25):
                print("---------- READ TEMPS ONLY -----------")
                unb.read_temp()
                unb.print_status()
                sleep(5)
            sleep(5)


if __name__ == "__main__":
    main()
