"""
Copyright 2022 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
ASTRON Netherlands Institute for Radio Astronomy
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


 Created: 2022-12-07
This file contains the definitions of the APSPU registers etc.


"""
#
# I2C address, registers and ports for APSCT
# Created: 2023-01-06
#

#
# Power supplies
#

PWR_LOCATIONS = {"INPUT":  0,
                 "PLL_160M": 1,
                 "PLL_200M": 2,
                 "DIST_A": 3,
                 "DIST_B": 4,
                 "DIST_C": 5,
                 "DIST_D": 6,
                 "CTRL": 7}

PWR_VOUT = {"INPUT":  3.3,
            "PLL_160M": 3.3,
            "PLL_200M": 3.3,
            "DIST_A": 3.3,
            "DIST_B": 3.3,
            "DIST_C": 3.3,
            "DIST_D": 3.3,
            "CTRL": 3.3}

#
# PLL constants / pin locations
#
CS = 6
SCLK = 4
SDO = 5
SDI = 7

PLL_200M = 0x20
PLL_160M = 0x21

#
# Central I2C Devices
#
EEPROM = 0x50

#
# I2C switch addresses
#
i2c_switch_addr_rcu = [0x70, 0x71, 0x72, 0x73]
i2c_bus_rcu = 1
i2c_switch_addr_unb = [0x70]
i2c_bus_unb = 3

#
# ID Pins 
#
ID_PINS = [8, 7, 12, 16, 20, 21]
