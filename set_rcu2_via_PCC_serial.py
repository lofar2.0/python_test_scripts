'''
Set RCU2 version via PCC. RCU number from commandline

'''
import sys
import time
sys.path.insert(0,'.')
import os
if os.name == "posix":
    from I2C_serial_pi import *
else:
    from I2C_serial import *

sleep_time=0.05

Nrcu=int(sys.argv[1]) if len(sys.argv)>1 else 0
version=sys.argv[2] if len(sys.argv)>2 else ""


EEPROM_ADDR = 0x50
SWITCH_ADDR = 0x70
I2CBUSNR = 1
LEN=16
REGISTER=0x20


def set_switch(addr_switch, RCU_nr):
    I2C_device = I2C(addr_switch, BUSNR=I2CBUSNR)
    I2C_device.write_register(1 << RCU_nr)
    ret_ack, ret_value = I2C_device.read_last_reg(1)
    print(ret_value)


def rw_eeprom(value):
    I2C_eeprom = I2C(EEPROM_ADDR, BUSNR=I2CBUSNR)
    if len(value)>0:
       for i,v in enumerate(bytearray(value)):
         print(i,hex(v))
         I2C_eeprom.write_bytes(i+REGISTER,v)
         sleep(0.1)
       for i in range(len(value),LEN):
         I2C_eeprom.write_bytes(i+REGISTER,0xff)
         sleep(0.1)
    v2=[]
    sleep(0.1)
    I2C_eeprom.write_register(REGISTER)
    sleep(0.1)
    ret_ack, ret_value = I2C_eeprom.read_last_reg(LEN)
    if ret_ack:
        stri = "EEPROM readback : {} ".format(ret_value)
        print(stri)
    else:
        print("READBACK failed")

if len(version)>LEN: version=version[:LEN]
print("Set RCU number %i version to '%s'" % (Nrcu,version));

set_switch(SWITCH_ADDR, Nrcu)
rw_eeprom(version)

