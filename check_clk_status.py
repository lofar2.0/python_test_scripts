'''
Copyright 2021 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
ASTRON Netherlands Institute for Radio Astronomy
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Set PCC_CLK

'''
import sys
import time
sys.path.insert(0,'.')
import os
if os.name =="posix":
    from I2C_serial_pi import *
else:
    from I2C_serial import *

I2CBUSNR=3
sleep_time=0.05
READ_LOCK = True

CS   = 6
SCLK = 4
SDO = 5
SDI = 7


def Read_byte_PLL(reg_address, nof_bytes=1, ADDRESS=0x20 ):
    #
    # Read Byte from the ADC
    #
    I2C_device = I2C(ADDRESS, BUSNR=I2CBUSNR)
    PLL_rw    = 0x01 # 0 for write, 1 for read

    I2C_device.write_bytes(0x06, 0x2C)
    data =  ( reg_address << 7 ) + PLL_rw

#    print("write read command")

    bit_array = "{0:{fill}8b}".format(data, fill='0')
    for bit in bit_array:
        for clk in range(2):
            Write_data = 0x02 | (0 << CS) | (clk << SCLK) | ( int(bit) << SDI)
            I2C_device.write_bytes(0x02, Write_data)
#            sleep(sleep_time)

#    print("read byte")
    read_bit = ''
    for cnt in range(8*nof_bytes):
        for clk in [0, 1]: # Read after rizing edge
            Write_data = 0x02 | (clk << SCLK) | ( int(bit) << SDI )
            I2C_device.write_bytes(0x02, Write_data)
        ret_ack, ret_value = I2C_device.read_bytes(0x00, 1)
#        stri= "ret_value = {}".format(int(ret_value,16))
#        print(stri)
        if ret_ack:
            read_bit += str((int(ret_value, 16) >> SDO) & 0x01)
        else:
            print("ACK nok")
    Write_data = 0x02 | (1 << CS) | (0 << SCLK) | (0 << SDI)
    I2C_device.write_bytes(0x02, Write_data)
    stri = "Read back at address 0x{0:{fill}2x} result : 0x{1:{fill}2x} ".format(reg_address, int(read_bit, 2), fill='0')
    print(stri)
    return read_bit;




if READ_LOCK:
    ret_value = Read_byte_PLL(0x00, nof_bytes = 1)
    status_pll = int(ret_value,2)
    if status_pll == 0x04:
        print("PLL in lock")
    elif (status_pll & 0x10) > 0:
        print("Not Locked --> No 10 MHz ref")
    else:
        print("Not locked --> PLL Error")

