""" Script for testing I2C monitor sensors

"""

from test_common import *
from time import sleep
connect("opc.tcp://10.87.6.89:4841/")

#POLS=["UNB2_POL_SWITCH_PHY","UNB2_POL_SWITCH_1V2", "UNB2_FPGA_POL_CORE"]
POLS=["UNB2_FPGA_POL_CORE", "UNB2_FPGA_POL_ERAM", "UNB2_FPGA_POL_RXGXB", "UNB2_FPGA_POL_TXGXB" , "UNB2_FPGA_POL_HGXB", "UNB2_FPGA_POL_PGM", "UNB2_DC_DC_48V_12V", "UNB2_POL_QSFP_N01", "UNB2_POL_QSFP_N23", "UNB2_POL_SWITCH_1V2", "UNB2_POL_SWITCH_PHY" , "UNB2_POL_CLOCK"] 
POINTS = ["VOUT", "IOUT", "TEMP"]

def check_values(values_array, atteribute_to_get):
    for check_value in values_array :
        if check_value > 50:
            stri = "Error in {} {}".format(atteribute_to_get, check_value)
            print(stri)

for loops in range(350):
    print(loops)
    for sens_cnt in range(len(POLS)*len(POINTS)):
        atteribute_to_get = POLS[int(sens_cnt/3)] + "_" + POINTS[sens_cnt%3] + "_R"
        ret_value = get_value(atteribute_to_get)
        check_values(ret_value, atteribute_to_get)
    sleep(5)
print("done")
