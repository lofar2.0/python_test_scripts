#******************************************#
# I2C address, registers and ports for UNB2c
# Created: 2021-05-11
#******************************************#

###################################
# General, Point of load converters
###################################
LOC_POWER_CORE    = 0x01
LOC_POWER_ERAM    = 0x0D
LOC_POWER_TR_R    = 0x0E
LOC_POWER_TR_T    = 0x0F
LOC_POWER_BAT     = 0x10
LOC_POWER_IO      = 0x11

LOC_POLS = {"LOC_POWER_CORE" : 0x01,
            "LOC_POWER_ERAM" : 0x0D,
            "LOC_POWER_TR_R" : 0x0E,
            "LOC_POWER_TR_T" : 0x0F,
            "LOC_POWER_BAT"  : 0x10,
            "LOC_POWER_IO"   : 0x11}

CTR_POWER_S_1V0   = 0x0E
CTR_POWER_S_1V2   = 0x0F
CTR_POWER_CLK     = 0x0D
CTR_POWER_QSFP_01 = 0x01
CTR_POWER_QSFP_23 = 0x02
CTR_POLS = {"CTR_POWER_S_1V0" :  0x0E,
            "CTR_POWER_S_1V2" : 0x0F,
            "CTR_POWER_CLK" : 0x0D,
            "CTR_POWER_QSFP_01" : 0x01,
            "CTR_POWER_QSFP_23" : 0x02}

LP_VOUT_MODE      = 0x20
LP_VOUT           = 0x8B #
LP_temp           = 0x8D #
LP_IOUT           = 0x8C

###################################
# Central I2C Devices
###################################
EEPROM            = 0x50


################
# Main I2C switch
################
MAIN_I2C_SWITCH   = 0x071
NODE=[]
NODE.append(0x01)
NODE.append(0x02)
NODE.append(0x04)
NODE.append(0x08)
FRONT_LED         = 0x20
CTRL_POWER        = 0x10

################
# Front panel LED
################
LED_DRIVER        = 0x41
LED_COLORS = {"red": 0x06,
              "blue": 0x05,
              "green" : 0x03,
              "magenta": 0x04,
              "yellow": 0x02,
              "cyaan": 0x01,
              "white": 0x00,
              "black" :0x07}

################
# Node I2C switch
################
NODE_I2C_SWITCH = 0x072
QSFP_PORT=[]
QSFP_PORT.append(0x08)
QSFP_PORT.append(0x04)
QSFP_PORT.append(0x02)
QSFP_PORT.append(0x01)
QSFP_PORT.append(0x40)
QSFP_PORT.append(0x80)
DDR4              = 0x10
LOC_POWER         = 0x20

###################################
# Node I2C Devices
###################################
QSFP_I2C_ADDR = 0x50
QSFP_TEMP = 0x16
QSFP_VOLT = 0x1A

MB_I_TEMP_I2C_ADDR = 0x18
MB_II_TEMP_I2C_ADDR = 0x19
MB_I_EEPROM = 0x50
MB_II_EEPROM = 0x51
MB_TEMP_REG = 0x05 # 5  #(Byte1*256+Byte2)*0.0625
MB_DATE = 0x143
MB_DATE_bytes = 2
MB_TYPE_NR = 0x149
MB_TYPE_NR_bytes = 20

######################
# Functions
######################

# Calculate floating point value according PMBus lineair
def calc_lin_2bytes(data):
    expo = ((data[1] & 0xf8)>>3) 
    if expo > 2**4:
        expo = expo-2**5
    mantisse = (data[1] & 0x7)*0x100 + data[0]
    output = mantisse * 2**expo
    return output

# Calculate floating point value according PMBus lineair
def calc_lin_3bytes(data,mode):
    expo = (mode[0] & 0x1F) 
    if expo > 2**4:
        expo = expo - 2**5
    output = (data[1]*256 + data[0]) * 2**expo
    return output
 
