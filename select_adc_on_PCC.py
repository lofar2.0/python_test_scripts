
'''
Copyright 2021 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
ASTRON Netherlands Institute for Radio Astronomy
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Set ADC

'''
import sys
import time
sys.path.insert(0,'.')
import os

if os.name =="posix":
    from I2C_serial_pi import *
else:
    from I2C_serial import *
    print("run on windows")


if (len(sys.argv) < 2) :
    print("\nUse: \npython select_adc_on_PCC.py <RCU_NR> \n")
else :
    RCU_nr = sys.argv[1]
    if RCU_Nr < 3:
        pr_stri = "Set RCU nr {}".format(RCU_nr)
        print(pr_stri)
        addr_switch = 0x70
        I2C_device = I2C(addr_switch)
        I2C_device.write_pointer(1 << RCU_nr)
    elif RCU_Nr == 99:
        pr_stri = "Set All RCU in parallel "
        print(pr_stri)
        addr_switch = 0x70
        I2C_device = I2C(addr_switch)
        I2C_device.write_pointer(0x0F)
    else:
        print("\nUse: \npython select_adc_on_PCC.py <RCU_NR 0..3 > \n")
