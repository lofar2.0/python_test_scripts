"""
Copyright 2022 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
ASTRON Netherlands Institute for Radio Astronomy
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


 Created: 2022-12-07
This file contains the definitions of the APSPU registers etc.


"""
#
# I2C address, registers and ports for APSPU
# Created: 2022-12-07
#

#
# General, Point of load converters
#

CTR_LBA = 0x3C
CTR_RCU2_A = 0x3D
CTR_RCU2_D = 0x0E
CTR_POLS = {"CTR_LBA":  0x3C,
            "CTR_RCU2_A": 0x3D,
            "CTR_RCU2_D": 0x3E}

VOUT_POLS = {"CTR_LBA":  8.0,
             "CTR_RCU2_A": 5.60009765625,
             "CTR_RCU2_D": 3.2998046875}

IOUT_POLS = {"CTR_LBA":  0.48,
             "CTR_RCU2_A": 0.6,
             "CTR_RCU2_D": 0.61}

STORE_PWD = {"CTR_LBA":  0x61,
            "CTR_RCU2_A": 0x4B,
            "CTR_RCU2_D": 0x35}

MAX_TEMP = 110

LP_VIN = 0x88
LP_VOUT_MODE = 0x20
LP_VOUT = 0x8B
LP_temp = 0x8D
LP_IOUT = 0x8C
LP_VOUT_COMMAND = 0x21
LP_VOUT_TRIM = 0x22
LP_VOUT_MAX = 0x24
LP_VOUT_SCALE_LOOP = 0x29
LP_VOUT_SCALE_MON = 0x2A
LP_VOUT_OV_LIMIT = 0x40
LP_VOUT_UV_LIMIT = 0x44
LP_STORE_USER_ALL = 0x15
LP_ON_OFF_CONFIG = 0x02
LP_OPERATION = 0x01
LP_WRITE_PROTECT = 0x10
LP_STORE_DEFAULT_ALL = 0x12
LP_OT_FAULT_LIMIT = 0x4F
#
# Central I2C Devices
#
EEPROM = 0x50

#
# FAN speed
#
MAX6620 = 0x29

REG_GLOBAL = 0x00
REG_TACH_MSP_REGS = [0x10, 0x12, 0x14]
REG_TACH_LSP_REGS = [0x11, 0x13, 0x15]
TACH_PERIODS = 16
TACH_COUNT_FREQ = 8192
FAN_TACHS = 1

RUN_MONITOR = 0x02
NOF_APS_FANS = 3

#
# Functions
#


def calc_lin_2bytes(data):
    #
    # Calculate floating point value according PMBus lineair
    #
    # input data (Byte array)
    # return value (float)
    #
    expo = ((data[1] & 0xf8) >> 3)
    if expo == 1:
        expo = -7
    if expo > (2**4):
        expo = expo-2**5
    mantisse = (data[1] & 0x7)*0x100 + data[0]
    if mantisse > (2**(8+2)):
        mantisse = mantisse - 2**(8 + 3)
    output = mantisse * 2**expo
    return output


def calc_lin_3bytes(data, mode):
    #
    # Calculate floating point value according PMBus lineair
    #
    # input data (Byte array)
    # return value (float)
    #
    expo = (mode[0] & 0x1F)
    if expo > 2**4:
        expo = expo - 2**5
    mant = (data[1]*256 + data[0]) 
    output = mant * 2**expo
    return output
