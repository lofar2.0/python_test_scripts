"""
I2C Class

"""
from usb_iss import UsbIss, defs
from time import *
import sys
sys.path.insert(0, 'c:\python34\lib\site-packages')

DEBUG = False
port = 'COM3'
iss = UsbIss()
i2c_speed = 100

try:
    iss.open(port)
    iss.setup_i2c(i2c_speed, True, None, None)
    iss.close()
except:
    print("Error opening the USB-I2C interface. Exit program")
    sys.exit()
    print("USB-I2C was already in use")


class I2C:

    def __init__(self, address=0x40):
        self.I2C_Address = address
        self.BUS_NR = 1    # not used for Laptop but to enable Pi multiple busses.

    def open_i2c(self):
        port_open = False
        try_cnt = 0
        while (not port_open) & (try_cnt < 3):
            try:
                iss.open(port)
                sleep(0.1)
                iss.setup_i2c(i2c_speed, True, None, None)
                port_open = True
            except:
                iss.close()
                port_open = False
        return port_open

    def read_bytes(self, register, bytes_to_read=2):
        if not self.open_i2c(): sys.exit()
        ret_value = ''
        try:
            read_values = iss.i2c.read(self.I2C_Address, register, bytes_to_read)
            for x in read_values:
                ret_value += hex(x)[2:]
            ret_ack = True
        except:
            print(f"Error reading data from 0x{self.I2C_Address:x} register 0x{register:x} nof bytes {bytes_to_read}")
            ret_ack = False
            ret_value = "999"
            sleep(1)
        ret_value = ret_value #.decode("utf-8")
        iss.close()
        return ret_ack, ret_value

    def read_last_reg(self, bytes_to_read):
        if not self.open_i2c(): sys.exit()
        ret_value = []
        for cnt in range(bytes_to_read):
            ret_value += hex(iss.i2c.read_single(self.I2C_Address))[2:]
        ret_value = ret_value
        ret_ack = 1
        iss.close()
        return ret_ack, ret_value
  
    def write_bytes(self, register, data):
        ret_ack = 0
        try_cnt = 0
        while (ret_ack == 0) & (try_cnt < 3):
            try_cnt += 1
            if not self.open_i2c(): sys.exit()
            if type(data) != list:
                data = [data]
            try:
                iss.i2c.write(self.I2C_Address, register, data)
                ret_ack = 1
            except:
                print(f"Error writing {data} to register 0x{register:x} of 0x{self.I2C_Address:x} try number: {try_cnt}")
                sleep(1)
                ret_ack = 0
            iss.close()
        return ret_ack

    def write_pointer(self, register):
        if not self.open_i2c(): sys.exit()
        try:
            iss.i2c.write_single(self.I2C_Address, register)
            ret_ack = 1
        except:
            print("Error writing data")
            sleep(1)
            ret_ack = 0
        iss.close()
        return ret_ack


if __name__ == "__main__":
    I2C_Device = I2C(0x40)
    I2C_Device.write_bytes(0x00, 0x00)
    ret_ack, ret_value = I2C_Device.read_bytes(0x8C, 2)
    print(ret_value)
