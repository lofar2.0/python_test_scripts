"""
Copyright 2021 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
ASTRON Netherlands Institute for Radio Astronomy
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


Created: 2021-05-19 by Leon Hiemstra, edited by Gijs
file used to read and write to switch registers

This script is made to run on a Raspberry pi where SPI port 0 is connected
to the EEPROM socket.

use for setting the switch
Python spi_switch_Unb2c.py set

use for information
Python spi_switch_Unb2c.py stat

"""

import time
import sys
import spidev

DEBUG=False
CRC = False #True # CRC or packet counter
# We only have SPI bus 0 available to us on the Pi
bus = 0

#Device is the chip select pin. Set to 0 or 1, depending on the connections
device = 0

# Enable SPI
spi = spidev.SpiDev()

# Open a connection to a specific bus and device (chip select pin)
spi.open(bus, device)

# Set SPI speed and mode
spi.max_speed_hz = 2000000
#spi.max_speed_hz = 50000
spi.mode = 1


cmd_normal_read = 0x60
cmd_normal_write = 0x61


def read_register(addr):
    #
    # Function to read from a SPI register
    #
    cmd =  cmd_normal_read
    if 0:
        spi.writebytes([cmd,addr])
        ret = spi.readbytes(2)
    else:
        ret = spi.xfer2([cmd,addr,0, 0, 0, 0])
    if DEBUG:
        stri = 'read_register  0x{:0>2x} = 0x{:0>2x}, 0x{:0>2x}'.format(addr, ret[2], ret[3])
        print(stri)
    return ret[2:]

def write_register(addr, data):
    #
    # Function to write to a SPI register
    #
    cmd =  cmd_normal_write
    if 0:
        spi.writebytes([cmd, addr, data])
        ret = spi.readbytes(2)
    else:
        ret = spi.xfer2([cmd,addr,data])
    if DEBUG:
        stri = 'write_register 0x{:0>2x} = 0x{:0>2x}'.format(addr, data)
        print(stri)

def read_switch(page, addr, pr_stri = True):
    #
    # Function to read from a register on the Switch
    #
    stri = '<< read switch from page: 0x{0:0>2x}, address: 0x{1:0>2x}'.format(page, addr)
    ret = spi.xfer2([cmd_normal_write, 0xff, page])
    ret = spi.xfer2([cmd_normal_read, addr, 0, 0, 0, 0])
    ret = read_register(0xfe)
    if (ret[2] & 0xf0) == 0xa0:
        ret = read_register(0xf0)
        ret.reverse()
        if pr_stri:
            stri += " data 0x"
            for byte in ret:
                stri += "{:0>2x}".format(byte) 
            print(stri)
    else:
        print("read error")
    return ret

def write_switch_bytes(page, addr, data, pr_stri = True):
    #
    # Function to write to a register on the Switch
    #
    stri = '> write switch from page: 0x{0:0>2x}, address: 0x{1:0>2x} data 0x'.format(page, addr)
    for byte_cnt in range(len(data)):
        add_stri = "{0:0>2x}".format(data[-1-byte_cnt])
        stri += add_stri
    if pr_stri:
        print(stri)
    read_register(0xfe)
    ret = spi.xfer2([cmd_normal_write, 0xff, page])
    wr_bytes = [cmd_normal_write, addr]
    wr_bytes.extend(data)
    ret = spi.xfer2(wr_bytes)
    ret = read_register(0xfe)[2]
    if ret != 0:
        print("write error, not enough words written")
    if DEBUG:
        read_register(0xfe)
        read_register(0xfe)
        read_register(addr)

def read_link_status():
    #
    # Function to read the port information on the switch
    #
    print("links status register")
    ret = read_switch(0x01,0x00, pr_stri=False)
    stri = "|15 |14 |13 |12 |11 |10 | 9 | 8 | 7 | 6 | 5 | 4 | 3 | 2 | 1 | 0 |"
    print(stri)
    stri = "| "
    for cnt_port in range(16):
        if ret[3]  & (0x8000 >> cnt_port) :
            stri += "P | "
        else:
            stri += "  | "
    print(stri)
    for cnt in [0,1,2,3]:
        stri = "Port status phy nr {} ".format(cnt)
        ret = read_switch(0x01,0x20+cnt, pr_stri = False)
        if ret[1] & 0x01:
            stri += "link up "
            if ret[1] & 0x02:
                stri += "dupplex "
            else:
                stri += "simplex "
            if ret[1] & 0x4:
                stri += " 100M "
            elif ret[1] & 0x8:
                stri += "1000M "
            else:
                stri += "  10M "
            ret = read_switch(0x10+cnt,0x28, pr_stri = False)
            if ret[1] & 0x01:
                stri += "SGMII  "
            else:
                stri += "SERDES "
            if ret[2] & 0x08:
                stri += "Tx: Er "
            if ret[2] & 0x04:
                stri += "Rx: Er "
            if ret[2] & 0x40:
                stri += "Rx FIFO Er "
#            ret_err = read_switch(0x10+cnt,0x2E, pr_stri = False)
#            stri += f"CRC errs: 0x{ret_err[1]:2x}"
#            rest = write_switch_bytes(0x01,0x23, [0x40], pr_stri = False)
        else:
            stri += "link down "
        print(stri)
# Read phy registister status
    for cnt in range(4):
        ret = read_switch(0x80+cnt,0x02, pr_stri = False)
        stri = " Phy status ch {} ".format(cnt)
        if ret[1] & 0x20 :
            stri += "AN_complete "
        if ret[1] & 0x04:
            stri += "link up "
            if ret[1] & 0x10:
                stri += "remote fault "
#            print(stri)
            ret = read_switch(0x80+cnt,0x14, pr_stri = False)
            stri += " link status ch{} ".format(cnt)
            if ret[2] & 0x10:
                stri += "remote status is good "
            else:
                stri += "remote status is NOK "
            if ret[2] & 0x20:
                stri += "local status is good "
            else:
                stri += "local status is NOK "
            if ret[2] & 0x40:
                stri += "local master"
            else:
                stri += "local slave"
        else:
            stri += " No link "
        print(stri)
        if 0:
            write_switch_bytes(0x80+cnt, 0x3c, [0x35, 0x08], pr_stri = False)
            ret = read_switch(0x80+cnt,0x3e)
    if 0:
        # Lines to read the received packets
        # Only works in combination with register on page 0x10+ch cont, addr: 0x20
        print("Receive count register") 
        for cnt in range(16):
            read_switch(0x10+cnt,0x2e)
    if 1:
        # alternative status read out, works better for SerDes lines.
        for prt_cnt in [8, 9, 10, 11,12, 13,14, 15]:
            ret = read_switch(0x10+prt_cnt,0x28, pr_stri = False)
            stri = "Port status of " + str(prt_cnt) + " "
            if ret[1] & 0x02:
                stri += "UP "
                if ret[1] & 0x01:
                    stri += "SGMII "
                else:
                    stri += "SerDes "
                if ret[1] & 0x10:
                    stri += "1000M "
                elif ret[1] & 0x08:
                    stri += "100M "
                else:
                    stri += "10M "
                if ret[1] & 0x80:
                    stri += "Link Changed "
                if ret[2] & 0x04:
                    stri += "Rx Err "
                if ret[2] & 0x08:
                    stri += "Tx Err "
                if ret[2] & 0x10:
                    stri += "CRC Err "
                if ret[2] & 0x40:
                    stri += "Rx Fifo Err "
                if ret[2] & 0x80:
                    stri += "Tx Fifo Err "
                ret = read_switch(0x10+prt_cnt,0x2E, pr_stri = False)
                if CRC:
                    rest = write_switch_bytes(0x10+prt_cnt,0x34, [0x20, 0x10], pr_stri = False)
                    rest = write_switch_bytes(0x10+prt_cnt,0x3e, [0x00, 0x00], pr_stri = False)
                    rest = write_switch_bytes(0x10+prt_cnt,0x20, [0xD0, 0x01], pr_stri = False)
                    stri += f"CRC err: {ret[0]} "
                else:
                    rest = write_switch_bytes(0x10+prt_cnt,0x3e, [0x00, 0x00], pr_stri = False)
                    rest = write_switch_bytes(0x10+prt_cnt,0x20, [0xD0, 0x09], pr_stri = False)
                    stri += f"Rx packet count: {ret[0]} "
                rest = write_switch_bytes(0x10+prt_cnt,0x22, [0x41, 0x41], pr_stri = False)
                rest = write_switch_bytes(0x10+prt_cnt,0x3e, [0x00, 0x00], pr_stri = False)
                rest = write_switch_bytes(0x10+prt_cnt,0x2E, [0x00, 0x00], pr_stri = False)
            else:
                stri += "Down "
            print(stri)
    print("strap resistors")
    read_switch(0x01,0x70)




if len(sys.argv) < 2:
    print("spi_switch_Unb2c stat for status")
    print("spi_switch_Unb2c set  to set registers")
elif sys.argv[1] == "stat":
    read_link_status()
elif sys.argv[1] == "set":
    # Setting for the switch
    print("Write and read jumbo register")
    write_switch_bytes(0x40, 0x01, [0xff, 0xff, 0x00, 0x00])
    if DEBUG:
        read_switch(0x40,0x01)
    print("Set PHY port to SGMII Master")
    for ch_cnt in range(4):
        write_switch_bytes(0x10 + ch_cnt, 0x20, [0xe0, 0x09])            
    print("Fix ports to 1000Mb/s")
    for cnt in [15, 14, 13, 12, 11, 10, 9, 8]: #, 0, 1, 2, 3]: # only ETH0 interface
        write_switch_bytes(0x00, 0x60+cnt, [0x8B]) #Fix FPGA links
    for cnt in [0, 1, 2, 3]: # only ETH0 interface
        write_switch_bytes(0x00, 0x60+cnt, [0x8B])
else:
    print("spi_switch_Unb2c stat for status")
    print("spi_switch_Unb2c set  to set registers")

spi.close()
