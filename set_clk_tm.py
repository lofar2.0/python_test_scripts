'''
Copyright 2021 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
ASTRON Netherlands Institute for Radio Astronomy
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Test script to write single registers of PCC_CLK.

'''
import sys
import time
sys.path.insert(0,'.')
import os
if os.name =="posix":
    from I2C_serial_pi import *
else:
    from I2C_serial import *

sleep_time=0.05
PWR_RST = False
SET_PLL = True
READ_LOCK = True
INT_POWER_CYCLE = False
RESET_PLL = False
UPDATE_PLL = False
READ_ALL = False

CS   = 6
SCLK = 4
SDO = 5
SDI = 7

def Write_byte_PLL(reg_address, wr_data, ADDRESS=0x20):
    #
    # Write Byte to the ADC
    #
    I2C_device = I2C(ADDRESS)
    PLL_rw    = 0x00 # 0 for write, 1 for read
    stri = "Write : 0x{0:{fill}2x} to Address : 0x{1:{fill}2x}".format(wr_data, reg_address, fill='0')
    print(stri)
    I2C_device.write_bytes(0x06, 0x2C)
    data =  ( reg_address << 9 ) + ( PLL_rw << 8 )+ wr_data
    
    bit_array = "{0:{fill}16b}".format(data, fill='0')
    I2C_device.write_bytes(0x02, 0x02 | (0x1 << CS))
    for bit in bit_array:
        for clk in range(2):
            Write_data = 0x02 | (0 << CS) | (clk << SCLK) | ( int(bit) << SDI)
            I2C_device.write_bytes(0x02, Write_data)
    for clk in range(2):
            Write_data = 0x02 | (0 << CS) | (clk << SCLK)
            I2C_device.write_bytes(0x02, Write_data)
    for clk in range(2):
            Write_data = 0x02 | (1 << CS) | (clk << SCLK)
            I2C_device.write_bytes(0x02, Write_data)

    Write_data = 0x02 | (1 << CS) | (0 << SCLK) | (0 << SDI)
    I2C_device.write_bytes(0x02, Write_data)

def Read_byte_PLL(reg_address, nof_bytes=1, ADDRESS=0x20 ):
    #
    # Read Byte from the ADC
    #
    I2C_device = I2C(ADDRESS)
    PLL_rw    = 0x01 # 0 for write, 1 for read

    I2C_device.write_bytes(0x06, 0x2C)
    data =  ( reg_address << 7 ) + PLL_rw

#    print("write read command")

    bit_array = "{0:{fill}8b}".format(data, fill='0')
    for bit in bit_array:
        for clk in range(2):
            Write_data = 0x02 | (0 << CS) | (clk << SCLK) | ( int(bit) << SDI)
            I2C_device.write_bytes(0x02, Write_data)
#            sleep(sleep_time)

#    print("read byte")
    read_bit = ''
    for cnt in range(8*nof_bytes):
        for clk in [0, 1]: # Read after rizing edge
            Write_data = 0x02 | (clk << SCLK) | ( int(bit) << SDI )
            I2C_device.write_bytes(0x02, Write_data)
        ret_ack, ret_value = I2C_device.read_bytes(0x00, 1)
#        stri= "ret_value = {}".format(int(ret_value,16))
#        print(stri)
        if ret_ack:
            read_bit += str((int(ret_value, 16) >> SDO) & 0x01)
        else:
            print("ACK nok")
    Write_data = 0x02 | (1 << CS) | (0 << SCLK) | (0 << SDI)
    I2C_device.write_bytes(0x02, Write_data)
    stri = "Read back at address 0x{0:{fill}2x} result : 0x{1:{fill}2x} ".format(reg_address, int(read_bit, 2), fill='0')
    print(stri)
    return read_bit;

def power(state):
    ADDRESS_IO = 0x20
    I2C_IO_device = I2C(ADDRESS_IO)
    I2C_IO_device.write_bytes(0x06, 0x2C)
    I2C_IO_device.write_bytes(0x07, 00)
    if state:
        bits_to_set = 0x42
    else:
        bits_to_set = 0x40
    I2C_IO_device.write_bytes(0x02, bits_to_set)


if PWR_RST :
    power(False)
    sleep(1)
    power(True)


if INT_POWER_CYCLE :
    print("Power OFF")
    Write_byte_PLL(0x03, 0x88) # Device down
    sleep(1)
    print("Power ON")
    Write_byte_PLL(0x03, 0x08) # Device up
    print("Done")

if RESET_PLL :
    print("Reset PLL")
    Write_byte_PLL(0x03, 0x0C) # Device reset
    print("Enable PLL")
    Write_byte_PLL(0x03, 0x08) # Device reset
    sleep(0.1)
    print("Done")

if SET_PLL :
    # Check PLL is in lock
#    Read_byte_PLL(0x00, nof_bytes = 23)

#    Write_byte_PLL(0x01, 0x00) # cp inv = 0xF4 other 0xE4
#    Write_byte_PLL(0x02, 0x04) # cp inv = 0xF4 other 0xE4
#    Write_byte_PLL(0x05, 0x97)
#    Write_byte_PLL(0x06, 0x10) # cp inv = 0xF4 other 0xE4
#    Write_byte_PLL(0x07, 0x04) # Divider R = 1 dec
#    Write_byte_PLL(0x08, 0x01)
#    Write_byte_PLL(0x07, 0x00)
#    Write_byte_PLL(0x09, 0x10) # Divider N = 20 dec
#    Write_byte_PLL(0x0A, 0x14)
#    Write_byte_PLL(0x09, 0x00)
    Write_byte_PLL(0x0D, 0x02) # Dig CLK     = 200/2 = 100 MHz
#    Write_byte_PLL(0x0C, 0x81)  # Dig CLK    delayed
#    Write_byte_PLL(0x0D, 0x01) # Dig CLK     = 200/1 = 200 MHz
#    Write_byte_PLL(0x0F, 0x01) # RCU CLK     = 200/1 = 200 MHz
#    Write_byte_PLL(0x11, 0x01) # PPS ref CLK = 200/1 = 200 MHz
#    Write_byte_PLL(0x13, 0x01) # T.P.    CLK = 200/1 = 200 MHz


if READ_LOCK:
    ret_value = Read_byte_PLL(0x00, nof_bytes = 1)
    status_pll = int(ret_value,2)
    if status_pll == 0x04:
        print("PLL in lock")
    elif (status_pll & 0x10) > 0:
        print("Not Locked --> No 10 MHz ref")
    else:
        print("Not locked --> PLL Error")

if READ_ALL:
    Read_byte_PLL(0x00, nof_bytes = 23)


if UPDATE_PLL:
    Write_byte_PLL(0x05, 0x97)
