'''
Copyright 2021 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
ASTRON Netherlands Institute for Radio Astronomy
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Set ADC

'''
import sys
import time
sys.path.insert(0,'.')
import os
if os.name =="posix":
    from I2C_serial_pi import *

else:
    from I2C_serial import *

I2CBUSNR=1
sleep_time=0.05
WRITE_DATA = True
READ_BYTE = True
SET_ADC = True

CS   = 0

SCLK = [1, 3, 5]
SDIO = [0, 2, 4]
ADC_ORDER = [0, 1, 2]


def Read_byte_ADC(ADC_reg_address, ADC_bytes=0, ADC_NR = 0, ADDRESS=0x20 ):
    #
    # Read Byte from the ADC
    #
    I2C_device = I2C(ADDRESS, BUSNR=I2CBUSNR)
    ADC_rw    = 0x01 # 0 for write, 1 for read
    
    stri = "Read ADC from Address {:8x}".format(ADC_reg_address)
    print(stri)
    
    data = ( ADC_rw << 15) + ( ADC_bytes << 13 ) + ADC_reg_address
    
#    print("write read command")
    I2C_device.write_bytes(0x06, 00)
    I2C_device.write_bytes(0x07, 00)
    I2C_device.write_bytes(0x03, (0x0F ^ (0x01 << ADC_NR)))

    bit_array = "{0:{fill}16b}".format(data, fill='0')
    for bit in bit_array:
        for clk in range(2):
            Write_data = 0x00 |  (clk << SCLK[ADC_NR]) | ( int(bit) << SDIO[ADC_NR] )
            I2C_device.write_bytes(0x02, Write_data)
#            sleep(sleep_time)
    I2C_device.write_bytes(0x03, 0x0F)
#    print("read byte")
    I2C_device.write_bytes(0x06, (1 << SDIO[ADC_NR]))
    I2C_device.write_bytes(0x03, (0x0F ^ (0x01 << ADC_NR)))
    read_bit = ''
    for cnt in range(8*(ADC_bytes+1)):
        for clk in [1,0]: # Read after faling edge
            Write_data = 0x00 | (clk << SCLK[ADC_NR]) | ( int(bit) << SDIO[ADC_NR] )
            I2C_device.write_bytes(0x02, Write_data)
        ret_ack, ret_value = I2C_device.read_bytes(0x00, 1)
        if ret_ack:
            read_bit += str((int(ret_value, 16) >> SDIO[ADC_NR])  & 0x01)
        else:
            print("ACK nok")
    I2C_device.write_bytes(0x03, 0x0F)
    I2C_device.write_bytes(0x07, 00)
    Write_data = 0x00 | (0 << SCLK[ADC_NR]) | ( 0 << SDIO[ADC_NR] )
    I2C_device.write_bytes(0x02, Write_data)
    I2C_device.write_bytes(0x03, 0x0F)
    stri = "Read back data is: {0:2x} ".format(int(read_bit, 2))
    print(stri)
    return read_bit;




for cnt in range(3):
    if SET_ADC :
        ADC_address = 0x3A # see address table
        ADC_data    = 0x00 # 8 bits data
        ADC_bytes   = 0x00 # 00 / 11 + 1 bytes
        ADCNR = ADC_ORDER[cnt]

        data = Read_byte_ADC(0x0A, ADC_NR = ADCNR)
        if data == "10000001":
            print("ADC locked")
        else:
            print("ADC not locked")


