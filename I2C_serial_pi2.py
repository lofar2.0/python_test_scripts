'''
Copyright 2021 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
ASTRON Netherlands Institute for Radio Astronomy
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

I2C_serial_Pi
Started by Gijs

Class for using the I2C bus of the I2C. This class is used for the
basic I2C scripts to read and write the RCU2, PCC etc.

'''
import pigpio
import sys
from time import *

DEBUG = False #True
SLOW = False

pi = pigpio.pi()

class I2C:

    def __init__(self, ADDRESS='040',BUSNR=3):
        self.I2C_Address = ADDRESS
        self.bus_nr = BUSNR

    def read_bytes(self, register, bytes_to_read=2):
        bus = pi.i2c_open(self.bus_nr, self.I2C_Address)
        try:
            (count, rd_value) = pi.i2c_read_i2c_block_data(bus, register, bytes_to_read)
            ret_value = ''
            if len(rd_value) < bytes_to_read:
                return False, 999
            for cnt in range(bytes_to_read):
                ret_value += (hex(rd_value[cnt])[2:])
            ret_ack = 1
            if SLOW:
                sleep(0.2)
        except IOError:
            ret_ack = 0
            ret_value = 'ffff'
            if DEBUG:
                print("Reading error")
        pi.i2c_close(bus)
        return ret_ack, ret_value

    
    def read_last_reg(self, bytes_to_read):
        bus = pi.i2c_open(self.bus_nr, self.I2C_Address)
        rd_value = []
        ret_value = ''
        try:
            (count, rd_value) = pi.i2c_read_device(bus, bytes_to_read)
            if DEBUG:
                print("read data")
                print(count)
                print(rd_value)
            ret_ack = 1
            if SLOW:
                sleep(0.2)
        except IOError:
            ret_ack = 0
            rd_value = [0]*bytes_to_read
            if DEBUG:
                print("Reading error")
        for cnt in range(bytes_to_read):
            ret_value += (hex(rd_value[cnt])[2:])
        pi.i2c_close(bus)
        return ret_ack,ret_value
  
    def write_bytes(self, register, data):
        bus = pi.i2c_open(self.bus_nr, self.I2C_Address)
        if type(data) is not list:
            data = [data]
        try:
            pi.i2c_write_i2c_block_data(bus, register, data)
            ret_ack = 1
            if SLOW:
                sleep(0.3)
        except IOError:
            ret_ack = 0
            ret_value = 0
            if DEBUG:
                print("Write error")
        pi.i2c_close(bus)
        return ret_ack

    def write_pointer(self, register):
        bus = pi.i2c_open(self.bus_nr, self.I2C_Address)
        try:
#            pi.i2c_read_device(bus, 1)
            pi.i2c_read_device(bus, register)
            ret_ack = 1
            if SLOW:
                sleep(0.3)
        except IOError:
            ret_ack = 0
            ret_value = 0
            if DEBUG:
                print("Write error")
        pi.i2c_close(bus)
        return ret_ack

    def write_register(self, register):
        bus = pi.i2c_open(self.bus_nr, self.I2C_Address)
        try:
            ret_value = pi.i2c_write_device(bus, [register])
            ret_ack = 1
            if SLOW:
                sleep(0.3)
        except IOError:
            ret_ack = 0
            ret_value = 0
            if DEBUG:
                print("No ACK")
        return ret_ack

    def ack_check(self):
        bus = smbus.SMBus(self.bus_nr)
        try:
            print("check ACK")
            ret_value = pi.i2c_write_quick(bus, 1)
            ret_ack = 1
            if SLOW:
                sleep(0.3)
        except IOError:
            ret_ack = 0
            ret_value = 0
            if DEBUG:
                print("No ACK")
        return ret_ack

if __name__ == "__main__":
    I2C_Device = I2C(0x40)
    I2C_Device.write_bytes(0x00, 0x00)
    ret_ack, ret_value = I2C_Device.read_bytes(0x8C, 2)
    print(ret_value)
