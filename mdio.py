# MDIO Interface to the ethernet Switch

import RPi.GPIO as GPIO
import time as time

ClockPin = 23
DataPin = 22
DEBUG = False

def write_mdio(phy_addr = 0x1E, phy_reg = 16, wr_data=0x16):

    print("write data")

    phy_addr_bits = "{:0>5b}".format(phy_addr)
    if DEBUG:
        stri = "Phy addr bits is {}".format(phy_addr_bits)
        print(stri)

    phy_reg_bits = "{:0>5b}".format(phy_reg)
    stri = "Phy reg bits is {}".format(phy_reg_bits)
    print(stri)

    phy_data_bits = "{:0>16b}".format(wr_data)
    stri = "Phy data bits is {}".format(phy_data_bits)
    print(stri)

    # Set IO pins to right mode
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(ClockPin, GPIO.OUT)
    GPIO.setup(DataPin, GPIO.OUT)

    # Preamble
    GPIO.output(DataPin, True)
    GPIO.output(ClockPin, False)
    for preamble_cnt in range(32):
        GPIO.output(ClockPin, True)
    #    time.sleep(0.1)
        GPIO.output(ClockPin, False)
    #    time.sleep(0.1)

    # Start of Frame
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)
    GPIO.output(DataPin, True)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)

    # Operant Write
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)
    GPIO.output(DataPin, True)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)

    # Phy address
    for bit in phy_addr_bits:
        if bit == '1':
            GPIO.output(DataPin, True)
        else:
            GPIO.output(DataPin, False)
        GPIO.output(ClockPin, True)
        GPIO.output(ClockPin, False)

    # phy reg
    for bit in phy_reg_bits:
        if bit == '1':
            GPIO.output(DataPin, True)
        else:
            GPIO.output(DataPin, False)
        GPIO.output(ClockPin, True)
        GPIO.output(ClockPin, False)

    # TA
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)

    #data
    for bit in phy_data_bits:
        if bit == '1':
            GPIO.output(DataPin, True)
        else:
            GPIO.output(DataPin, False)
        GPIO.output(ClockPin, True)
        GPIO.output(ClockPin, False)

def read_mdio(phy_addr = 0x1E, phy_reg = 16):
    print("read data")

    phy_reg_bits = "{:0>5b}".format(phy_reg)
    stri = "Phy reg bits is {}".format(phy_reg_bits)
    print(stri)

    phy_addr_bits = "{:0>5b}".format(phy_addr)
    if DEBUG:
        stri = "Phy addr bits is {}".format(phy_addr_bits)
        print(stri)

    # Set IO pins to right mode
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(ClockPin, GPIO.OUT)
    GPIO.setup(DataPin, GPIO.OUT)

    # Preamble
    GPIO.output(DataPin, True)
    GPIO.output(ClockPin, False)
    for preamble_cnt in range(32):
        GPIO.output(ClockPin, True)
        #    time.sleep(0.1)
        GPIO.output(ClockPin, False)
    #    time.sleep(0.1)

    # Start of Frame
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)
    GPIO.output(DataPin, True)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)

    # Operant Read
    GPIO.output(DataPin, True)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)

    # Phy address
    for bit in phy_addr_bits:
        if bit == '1':
            GPIO.output(DataPin, True)
        else:
            GPIO.output(DataPin, False)
        GPIO.output(ClockPin, True)
        GPIO.output(ClockPin, False)

    # phy reg
    for bit in phy_reg_bits:
        if bit == '1':
            GPIO.output(DataPin, True)
        else:
            GPIO.output(DataPin, False)
        GPIO.output(ClockPin, True)
        GPIO.output(ClockPin, False)

    GPIO.setup(DataPin, GPIO.IN)
    # TA
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)

    # data
    data=[]
    for bit_cnt in range(16):
        GPIO.output(ClockPin, True)
        data.append(GPIO.input(DataPin))
        GPIO.output(ClockPin, False)

    print(data)
    return data

def write_mdio_45(phy_addr = 0x1E, phy_reg = 16, wr_data=0x16):

    print("write data in 45 mode")

    phy_reg_bits = "{:0>5b}".format(phy_reg)
    stri = "Phy reg bits is {}".format(phy_reg_bits)
    print(stri)

    phy_addr_bits = "{:0>5b}".format(phy_addr)
    if DEBUG:
        stri = "Phy addr bits is {}".format(phy_addr_bits)
        print(stri)

    phy_data_bits = "{:0>16b}".format(wr_data)
    stri = "Phy data bits is {}".format(phy_data_bits)
    print(stri)

    # Set IO pins to right mode
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(ClockPin, GPIO.OUT)
    GPIO.setup(DataPin, GPIO.OUT)

    # Preamble
    GPIO.output(DataPin, True)
    GPIO.output(ClockPin, False)
    for preamble_cnt in range(32):
        GPIO.output(ClockPin, True)
    #    time.sleep(0.1)
        GPIO.output(ClockPin, False)
    #    time.sleep(0.1)

    # Start of Frame
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)

    # Operant Write
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)
    GPIO.output(DataPin, True)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)

    # Phy address
    for bit in phy_addr_bits:
        if bit == '1':
            GPIO.output(DataPin, True)
        else:
            GPIO.output(DataPin, False)
        GPIO.output(ClockPin, True)
        GPIO.output(ClockPin, False)

    # phy reg
    for bit in phy_reg_bits:
        if bit == '1':
            GPIO.output(DataPin, True)
        else:
            GPIO.output(DataPin, False)
        GPIO.output(ClockPin, True)
        GPIO.output(ClockPin, False)

    # TA
    GPIO.output(DataPin, True)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)

    #data
    for bit in phy_data_bits:
        if bit == '1':
            GPIO.output(DataPin, True)
        else:
            GPIO.output(DataPin, False)
        GPIO.output(ClockPin, True)
        GPIO.output(ClockPin, False)

def read_mdio_45(phy_addr = 0x1E, phy_reg = 16):
    print("read data in 45 mode")

    phy_reg_bits = "{:0>5b}".format(phy_reg)
    stri = "Phy reg bits is {}".format(phy_reg_bits)
    print(stri)

    phy_addr_bits = "{:0>5b}".format(phy_addr)
    if DEBUG:
        stri = "Phy addr bits is {}".format(phy_addr_bits)
        print(stri)

    # Set IO pins to right mode
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(ClockPin, GPIO.OUT)
    GPIO.setup(DataPin, GPIO.OUT)

    # Preamble
    GPIO.output(DataPin, True)
    GPIO.output(ClockPin, False)
    for preamble_cnt in range(32):
        GPIO.output(ClockPin, True)
        #    time.sleep(0.1)
        GPIO.output(ClockPin, False)
    #    time.sleep(0.1)

    # Start of Frame
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)

    # Operant Read
    GPIO.output(DataPin, True)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)
    GPIO.output(DataPin, True)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)

    # Phy address
    for bit in phy_addr_bits:
        if bit == '1':
            GPIO.output(DataPin, True)
        else:
            GPIO.output(DataPin, False)
        GPIO.output(ClockPin, True)
        GPIO.output(ClockPin, False)

    # phy reg
    for bit in phy_reg_bits:
        if bit == '1':
            GPIO.output(DataPin, True)
        else:
            GPIO.output(DataPin, False)
        GPIO.output(ClockPin, True)
        GPIO.output(ClockPin, False)

    # TA
    GPIO.output(DataPin, True)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)

    GPIO.setup(DataPin, GPIO.IN)

    # data
    data=[]
    for bit_cnt in range(16):
        GPIO.output(ClockPin, True)
        data.append(GPIO.input(DataPin))
        GPIO.output(ClockPin, False)

    print(data)
    return data

def addr_mdio_45(phy_addr = 0x1E, phy_reg = 16, wr_addr=0x16):

    print("write address in 45 mode")

    phy_reg_bits = "{:0>5b}".format(phy_reg)
    stri = "Phy reg bits is {}".format(phy_reg_bits)
    print(stri)

    phy_addr_bits = "{:0>5b}".format(phy_addr)
    if DEBUG:
        stri = "Phy addr bits is {}".format(phy_addr_bits)
        print(stri)

    phy_data_bits = "{:0>16b}".format(wr_addr)
    stri = "Phy data bits is {}".format(phy_data_bits)
    print(stri)

    # Set IO pins to right mode
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(ClockPin, GPIO.OUT)
    GPIO.setup(DataPin, GPIO.OUT)

    # Preamble
    GPIO.output(DataPin, True)
    GPIO.output(ClockPin, False)
    for preamble_cnt in range(32):
        GPIO.output(ClockPin, True)
    #    time.sleep(0.1)
        GPIO.output(ClockPin, False)
    #    time.sleep(0.1)

    # Start of Frame
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)

    # Operant Addr
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)

    # Phy address
    for bit in phy_addr_bits:
        if bit == '1':
            GPIO.output(DataPin, True)
        else:
            GPIO.output(DataPin, False)
        GPIO.output(ClockPin, True)
        GPIO.output(ClockPin, False)

    # phy reg
    for bit in phy_reg_bits:
        if bit == '1':
            GPIO.output(DataPin, True)
        else:
            GPIO.output(DataPin, False)
        GPIO.output(ClockPin, True)
        GPIO.output(ClockPin, False)

    # TA
    GPIO.output(DataPin, True)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)
    GPIO.output(DataPin, False)
    GPIO.output(ClockPin, True)
    GPIO.output(ClockPin, False)

    #data
    for bit in phy_data_bits:
        if bit == '1':
            GPIO.output(DataPin, True)
        else:
            GPIO.output(DataPin, False)
        GPIO.output(ClockPin, True)
        GPIO.output(ClockPin, False)

def read_switch(page = 1, addr = 8):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(DataPin, GPIO.OUT)
    GPIO.setup(ClockPin, GPIO.OUT)
    reg_16_data = (page << 8) + 1
    reg_17_data = (addr << 8) + 0
    reg_17b_data = (addr << 8) + 2
    write_mdio(phy_addr = 0x1E, phy_reg = 16, wr_data=reg_16_data)
    read_mdio(phy_addr = 0x1E, phy_reg = 16)
    write_mdio(phy_addr = 0x1E, phy_reg = 17, wr_data=reg_17_data)
    read_mdio(phy_addr = 0x1E, phy_reg = 17)
    write_mdio(phy_addr = 0x1E, phy_reg = 17, wr_data=reg_17b_data)
    read_mdio(phy_addr = 0x1E, phy_reg = 17)
    ret = read_mdio(phy_addr = 0x1E, phy_reg = 17)
    GPIO.setup(DataPin, GPIO.OUT)
    for cnt in range(1):
        GPIO.output(DataPin, False)
        for clk_cnt in range(16):
                GPIO.output(ClockPin, True)
                GPIO.output(ClockPin, False)
        if ret[-2:] != [0, 0]:
            ret = read_mdio(phy_addr = 0x1E, phy_reg = 17)
    read_mdio(phy_addr = 0x1E, phy_reg = 24)
    read_mdio(phy_addr = 0x1E, phy_reg = 25)
    read_mdio(phy_addr = 0x1E, phy_reg = 26)
    read_mdio(phy_addr = 0x1E, phy_reg = 27)
    write_mdio(phy_addr = 0x1E, phy_reg = 17, wr_data=0x0000)

def write_switch(page = 1, addr = 8, data = 0x55AA):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(DataPin, GPIO.OUT)
    GPIO.setup(ClockPin, GPIO.OUT)
    reg_16_data = (page << 8) + 1
    reg_17_data = (addr << 8) + 0
    reg_17b_data = (addr << 8) + 1
    write_mdio(phy_addr = 0x1E, phy_reg = 16, wr_data=reg_16_data)
    read_mdio(phy_addr = 0x1E, phy_reg = 16)
    write_mdio(phy_addr = 0x1E, phy_reg = 17, wr_data=reg_17_data)
    read_mdio(phy_addr = 0x1E, phy_reg = 17)
    write_mdio(phy_addr = 0x1E, phy_reg = 24, wr_data=(data & 0xffff))
    read_mdio(phy_addr = 0x1E, phy_reg = 24)
    write_mdio(phy_addr = 0x1E, phy_reg = 25, wr_data=((data >> 16) & 0xffff))
    read_mdio(phy_addr = 0x1E, phy_reg = 25)
    write_mdio(phy_addr = 0x1E, phy_reg = 26, wr_data=((data >> 32) & 0xffff))
    read_mdio(phy_addr = 0x1E, phy_reg = 26)
    write_mdio(phy_addr = 0x1E, phy_reg = 27, wr_data=((data >> 48) & 0xffff))
    read_mdio(phy_addr = 0x1E, phy_reg = 27)
    write_mdio(phy_addr = 0x1E, phy_reg = 17, wr_data=reg_17_data)
    read_mdio(phy_addr = 0x1E, phy_reg = 17)
    write_mdio(phy_addr = 0x1E, phy_reg = 17, wr_data=reg_17b_data)
    read_mdio(phy_addr = 0x1E, phy_reg = 17)
    ret = read_mdio(phy_addr = 0x1E, phy_reg = 17)
    GPIO.setup(DataPin, GPIO.OUT)
    for cnt in range(1):
        GPIO.output(DataPin, False)
        for clk_cnt in range(16):
                GPIO.output(ClockPin, True)
                GPIO.output(ClockPin, False)
        if ret[-2:] != [0, 0]:
            ret = read_mdio(phy_addr = 0x1E, phy_reg = 17)

if 0:
    # Write to a switch regiter
    print("------------ Write -----------")
    write_switch(page = 34, addr = 10, data = 0x55AA)

if 1:
    # read from switch register
    print("------------ Read -----------")
    read_switch(page = 1, addr = 70)

if 0:
    # Read a phy register
    write_mdio(phy_addr = 0x00, phy_reg = 0x17, wr_data = 0x0F7E)
    read_mdio(phy_addr = 0x00, phy_reg = 0x17)

GPIO.cleanup()
